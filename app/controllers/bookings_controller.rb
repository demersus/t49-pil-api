class BookingsController < ApplicationController
  def show
    render json: {booking: Booking.find(params[:booking_number])}
  end
end

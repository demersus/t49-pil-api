class ApplicationController < ActionController::API
  rescue_from ApiRecordNotFound, with: :error_404

  private
  def error_404
    render json: {error: "not found"}, status: 404
  end
end

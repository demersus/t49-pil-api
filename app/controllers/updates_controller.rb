class UpdatesController < ApplicationController
  def index
    render json: {updates: Update.find_all(**_find_params)}
  end

  private
  def _find_params
    keys = [:booking_number, :container_number]

    Hash[ [
      keys,
      params.require(keys)
    ].transpose ]
  end
end

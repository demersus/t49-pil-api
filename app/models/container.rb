class Container
  include ActiveModel::Serialization

  attr_accessor :container_number, :container_type, :movement_type,
    :latest_event, :place, :date

  def initialize(fields={})
    set_attributes fields
  end

  def set_attributes attrs
    attrs.each do |key, value|
      self.send "#{key}=", value
    end
  end
end

require 'net/http'
require 'json'
require 'nokogiri'

class Booking
  include ActiveModel::Serialization

  attr_accessor :booking_number, :containers, :place_of_receipt,
    :place_of_delivery, :number_of_containers,
    :arrival, :delivery, :vessel, :voyage, :location, :next_location,
    :next_location_date, :containers, :steamship_line

  def initialize(fields={})
    set_attributes fields
    @steamship_line ||= "PIL"
  end

  def set_attributes attrs
    attrs.each do |key, value|
      self.send "#{key}=", value
    end
  end

  def self.find(booking_number)
    data = self.get_data_from_api(booking_number)

    self.new(
      _api_data: data
    )
  end

  def self.get_data_from_api(id)
    request_url = "https://www.pilship.com/shared/ajax/?fn=get_tracktrace_bl&ref_num=#{id}"
    response = Net::HTTP.get(URI(request_url)).force_encoding('UTF-8')

    encoding_bom = "\xEF\xBB\xBF"
    data = JSON.parse(response.gsub(encoding_bom, ''), symbolize_names: true)

    if data[:data].include?(:err) && data[:data][:err].to_i > 0
      raise ApiRecordNotFound.new "Api record for Booking/#{id} was not found"
    else
      data
    end
  end


  private

  def _api_data=data
    set_attributes  _parse_refnum_info      data[:data][:refnum_info]
    set_attributes  _parse_schedule_table   data[:data][:scheduletable]
    set_attributes  _parse_schedule_info    data[:data][:scheduleinfo]
    set_attributes  _parse_containers       data[:data][:containers]
  end

  def _parse_refnum_info(html)
    return {
      booking_number: html[/.*(TXG\d+).*/, 1]
    }
  end

  def _parse_schedule_table(html)
    fragment = Nokogiri::HTML.fragment(html)

    return {
      arrival: fragment.css('td.arrival-delivery')[0].inner_html.split("<br>")[1],
      delivery: fragment.css('td.arrival-delivery')[0].inner_html.split("<br>")[2],
      location: fragment.css('td.location')[0].inner_html.gsub("<br>", ' '),
      vessel: fragment.css('td.vessel-voyage')[0].inner_html.split("<br>")[1],
      voyage: fragment.css('td.vessel-voyage')[0].inner_html.split("<br>")[2],
      next_location: fragment.css('td.next-location')[0].inner_html.split("<br>")[1],
      next_location_date: fragment.css('td.next-location')[0].inner_html.split("<br>")[2],
    }
  end

  def _parse_schedule_info(html)
    data = html.split('<br />').inject({}) do |hash, fragment|
      # frament will be in format: "key <b>value</b>"
      parsed_fragment = Nokogiri::HTML.fragment(fragment)
      value = parsed_fragment.xpath("b/text()").first.to_s.strip

      parsed_fragment.search("b").remove
      key = parsed_fragment.to_s.strip
      hash[key] = value
      hash
    end

    return {
      place_of_receipt: data['Place of Receipt'],
      place_of_delivery: data['Place of Delivery'],
      number_of_containers: data['No. of Containers']
    }
  end

  def _parse_containers(html)
    fragment = Nokogiri::HTML.fragment(html)
    {
      #containers_html: html,
      containers: fragment.css('tr.resultrow').map do |tr|
        Container.new( _parse_container_row(tr) )
      end
    }
  end

  def _parse_container_row(row)
    {
      container_number: row.css('td.container-num > b').inner_text,
      container_type: row.css('td.container-type').inner_text,
      movement_type: row.css('td.movement').inner_text,
      date: row.css('td.date').inner_text,
      latest_event: row.css('td.latest-event').inner_text,
      place: row.css('td.place').inner_text
    }
  end

end

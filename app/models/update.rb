require 'net/http'
require 'json'
require 'nokogiri'

class Update
  include ActiveModel::Serialization

  attr_accessor :container_number, :booking_number, :date, :latest_event, :place

  def initialize(fields={})
    set_attributes fields
  end

  def set_attributes attrs
    attrs.each do |key, value|
      self.send "#{key}=", value
    end
  end

  def self.find_all(booking_number:, container_number:)
    data = self.get_data_from_api(booking_number, container_number)

    self._build_updates_from_html(data[:data][:events_table]).each do |update|
      update.set_attributes booking_number: booking_number,
                            container_number: container_number
    end
  end

  def self.get_data_from_api(booking_number, container_number)
    request_url = "https://www.pilship.com/shared/ajax/?fn=get_track_container_status&search_type=bl&search_type_no=#{booking_number}&ref_num=#{container_number}"
    response = Net::HTTP.get(URI(request_url)).force_encoding('UTF-8')

    encoding_bom = "\xEF\xBB\xBF"

    JSON.parse(response.gsub(encoding_bom, ''), symbolize_names: true)
  end

  private
  def self._build_updates_from_html(html)
    fragment = Nokogiri::HTML.fragment(html)

    fragment.search('tr.subresultrow').map do |nodeset|
      self.new _attrs_from_result_row_html(nodeset)
    end
  end

  def self._attrs_from_result_row_html row
    {
      date: row.search('td.date').inner_text,
      latest_event: row.search('td.latest-event').inner_text,
      place: row.search('td.place').inner_text
    }
  end
end

FROM ruby:alpine
RUN gem install bundler
RUN apk add --update --no-cache tzdata build-base libxml2-dev libxslt-dev libbz2 libevent-dev libffi-dev glib-dev ncurses-dev readline-dev yaml-dev zlib-dev
RUN mkdir /app
ADD . /app
WORKDIR /app/
RUN bundle --path vendor/bundle


EXPOSE 3000
CMD ["bin/rails","s"]

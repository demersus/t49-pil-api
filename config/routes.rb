Rails.application.routes.draw do
  scope :api do
    get '/bookings/:booking_number' => 'bookings#show'
    get '/updates' => 'updates#index'
  end
end

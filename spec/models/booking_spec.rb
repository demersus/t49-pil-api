require 'rails_helper'

RSpec.describe Booking, type: :model do

  describe '.find' do

    let(:booking_response) {
      "\xEF\xBB\xBF{\"error\":{\"type\":0,\"msg\":\"FN not set!\"},\"request\":\"Array\\n(\\n    [fn] => get_tracktrace_bl\\n    [ref_num] => TXG790214500\\n)\\n\",\"data\":{\"err\":0,\"err_msg\":\"\",\"refnum_info\":\"B\\/L Number: <b>TXG790214500<\\/b><br \\/>\",\"scheduletable\":\"<tr class=\\\"resultrow\\\"><td class=\\\"arrival-delivery\\\"><br \\/>2017-04-18<br \\/>2017-04-19<\\/td><td class=\\\"location\\\">Load Port<br \\/>XINGANG<br \\/>CNTXG<\\/td><td class=\\\"vessel-voyage\\\"><br \\/>CSCL AUTUMN<br \\/>VQC60007E<\\/td><td class=\\\"next-location\\\"><br \\/>USOAK<br \\/>2017-05-15<\\/td><\\/tr>\",\"containers\":\"<tr class=\\\"titles\\\"><td class=\\\"container-num\\\">Container #<\\/td><td class=\\\"container-type\\\">Size\\/Type<\\/td><td class=\\\"movement\\\">Movement Type<\\/td><td class=\\\"date\\\">Date<\\/td><td class=\\\"latest-event\\\">Latest Event<\\/td><td class=\\\"place\\\">Place<\\/td><\\/tr><tr class=\\\"resultrow\\\" id=\\\"wrapper_PCIU8822077\\\"><td colspan=\\\"6\\\"><table><tr><td class=\\\"container-num\\\"><b>PCIU8822077<\\/b> <a class=\\\"trackinfo\\\" href=\\\"javascript:void(0);\\\" name=\\\"trackinfo::bl::TXG790214500::PCIU8822077\\\"><b>Trace<\\/b><\\/a> <span class=\\\"subsearch-ajax hidden\\\"><\\/span><\\/td><td class=\\\"container-type\\\">40HC<\\/td><td class=\\\"movement\\\">FCL\\/FCL<\\/td><td class=\\\"date\\\">2017-05-19 10:48:00<\\/td><td class=\\\"latest-event\\\">Empty Container<\\/td><td class=\\\"place\\\">OAKLAND<\\/td><\\/tr><\\/table><table id=\\\"subresult_wrapper_PCIU8822077\\\" class=\\\"big_subresult_wrapper hidden\\\"><tbody class=\\\"big_subresult_main\\\" id=\\\"big_subresult_main_PCIU8822077\\\"><\\/tbody><\\/table><\\/td><\\/tr>\",\"scheduleinfo\":\"Place of Receipt <b>XINGANG [CNTXG]<\\/b><br \\/>Place of Delivery <b>OAKLAND [USOAK]<\\/b><br \\/>No. of Containers<b>1 x 40HC<\\/b><br \\/>\"}}"
    }

    let(:booking_number) { "TXG790214500" }

    subject { Booking.find(booking_number) }

    before do
      @api_stub = stub_request(:get, "https://www.pilship.com/shared/ajax/?fn=get_tracktrace_bl&ref_num=#{booking_number}").and_return({
        body: booking_response
      })
    end

    it 'requests the expected data endpoint' do
      subject
      expect(@api_stub).to have_been_requested
    end

    it 'returns a booking object with expected fields from api response' do
      expect(subject).to be_a Booking

      expected_fields = {
        booking_number: booking_number,
        place_of_receipt: "XINGANG [CNTXG]",
        place_of_delivery: "OAKLAND [USOAK]",
        number_of_containers: "1 x 40HC",
        arrival: "2017-04-18",
        delivery: "2017-04-19",
        location: "Load Port XINGANG CNTXG",
        vessel: "CSCL AUTUMN",
        voyage: "VQC60007E",
        next_location: "USOAK",
        next_location_date: "2017-05-15",
      }
      expected_fields.each do |name, value|
        expect(subject.send(name)).to eql value
      end
    end

    it 'parses the Containers' do
      expect(subject.containers.first).to be_a Container

      expected_fields = {
        container_number: "PCIU8822077",
        container_type: "40HC",
        movement_type: "FCL/FCL",
        date: "2017-05-19 10:48:00",
        latest_event: "Empty Container",
        place: "OAKLAND"
      }

      expected_fields.each do |name, value|
        expect(subject.containers.first.send(name)).to eql value
      end
    end
  end
end

require 'rails_helper'

RSpec.describe Update, type: :model do

  describe '.find' do
    let(:updates_response) {
      "\xEF\xBB\xBF{\"error\":{\"type\":0,\"msg\":\"FN not set!\"},\"request\":\"Array\\n(\\n    [fn] => get_track_container_status\\n    [search_type] => bl\\n    [search_type_no] => TXG790214800\\n    [ref_num] => PCIU8757536\\n    [_] => 1514009238279\\n)\\n\",\"data\":{\"events_table\":\"<tr class=\\\"subresultrow\\\"><td class=\\\"container-num\\\" style=\\\"width: 405px;\\\"> \\/ <\\/td><td class=\\\"date\\\" style=\\\"width: 105px;\\\">2017-04-12 00:00:00<\\/td><td class=\\\"latest-event\\\" style=\\\"width: 210px;\\\">Empty Release for outbound<\\/td><td class=\\\"place\\\" style=\\\"width: 100px;\\\">XINGANG<\\/td><\\/tr><tr class=\\\"subresultrow\\\"><td class=\\\"container-num\\\" style=\\\"width: 405px;\\\"> \\/ <\\/td><td class=\\\"date\\\" style=\\\"width: 105px;\\\">2017-04-19 23:30:00<\\/td><td class=\\\"latest-event\\\" style=\\\"width: 210px;\\\">Outbound Full CY<\\/td><td class=\\\"place\\\" style=\\\"width: 100px;\\\">XINGANG<\\/td><\\/tr><tr class=\\\"subresultrow\\\"><td class=\\\"container-num\\\" style=\\\"width: 405px;\\\">CSCL AUTUMN \\/ VQC60007E<\\/td><td class=\\\"date\\\" style=\\\"width: 105px;\\\">2017-04-19 23:30:00<\\/td><td class=\\\"latest-event\\\" style=\\\"width: 210px;\\\">Vessel Loading<\\/td><td class=\\\"place\\\" style=\\\"width: 100px;\\\">XINGANG<\\/td><\\/tr><tr class=\\\"subresultrow\\\"><td class=\\\"container-num\\\" style=\\\"width: 405px;\\\">CSCL AUTUMN \\/ VQC60007E<\\/td><td class=\\\"date\\\" style=\\\"width: 105px;\\\">2017-05-11 15:28:00<\\/td><td class=\\\"latest-event\\\" style=\\\"width: 210px;\\\">Vessel discharge<\\/td><td class=\\\"place\\\" style=\\\"width: 100px;\\\">LONG BEACH<\\/td><\\/tr><tr class=\\\"subresultrow\\\"><td class=\\\"container-num\\\" style=\\\"width: 405px;\\\"> \\/ <\\/td><td class=\\\"date\\\" style=\\\"width: 105px;\\\">2017-05-11 15:28:00<\\/td><td class=\\\"latest-event\\\" style=\\\"width: 210px;\\\">Inbound Full CY<\\/td><td class=\\\"place\\\" style=\\\"width: 100px;\\\">LONG BEACH<\\/td><\\/tr><tr class=\\\"subresultrow\\\"><td class=\\\"container-num\\\" style=\\\"width: 405px;\\\"> \\/ <\\/td><td class=\\\"date\\\" style=\\\"width: 105px;\\\">2017-05-16 18:15:00<\\/td><td class=\\\"latest-event\\\" style=\\\"width: 210px;\\\">Inbound Full Delivery<\\/td><td class=\\\"place\\\" style=\\\"width: 100px;\\\">LONG BEACH<\\/td><\\/tr><tr class=\\\"subresultrow\\\"><td class=\\\"container-num\\\" style=\\\"width: 405px;\\\"> \\/ <\\/td><td class=\\\"date\\\" style=\\\"width: 105px;\\\">2017-05-19 15:28:00<\\/td><td class=\\\"latest-event\\\" style=\\\"width: 210px;\\\">Empty Container<\\/td><td class=\\\"place\\\" style=\\\"width: 100px;\\\">LOS ANGELES<\\/td><\\/tr>\"}}"
    }

    let(:booking_number) { "TXG790214500" }
    let(:container_number) { "PCIU8757536" }

    subject { Update.find_all(booking_number: booking_number, container_number: container_number) }

    before do
      @api_stub = stub_request(:get, "https://www.pilship.com/shared/ajax/?fn=get_track_container_status&search_type=bl&search_type_no=#{booking_number}&ref_num=#{container_number}").and_return({
        body: updates_response
      })
    end

    it 'requests the expected data endpoint' do
      subject
      expect(@api_stub).to have_been_requested
    end

    it 'returns an Update model with expected fields' do
      expect(subject.first).to be_a Update

      expected_fields = {
        container_number: container_number,
        booking_number: booking_number,
        date: "2017-04-12 00:00:00",
        place: 'XINGANG',
        latest_event: 'Empty Release for outbound'
      }

      expected_fields.each do |name, value|
        expect(subject.first.send(name)).to eql value
      end
    end
  end
end
